package ru.kosuhina_sy.nlmk;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class DescriptionListMaker {

    //private static final Logger logger = LogManager.getLogger(DescriptionListMaker.class);

    public List<List<Description>> makeDescriptionList(List<Object> objects) {
        List<List<Description>> result = new ArrayList<>();
        Class testClass = objects.get(0).getClass();
        for(Object object :objects) {
            if (testClass !=object.getClass()) {
                throw new IllegalArgumentException();
            }
            Class clazz = object.getClass();
            do {
                List<Description> descriptionList = new ArrayList<>();
                parseClass(clazz.getDeclaredFields(), descriptionList, object);
                if(descriptionList.size() > 0) result.add(descriptionList);
                clazz = clazz.getSuperclass();
            } while (clazz.getSuperclass() != null);
        }
        return result;

    }
private void parseClass(Field[] fields, List<Description> descriptionList, Object object) {
        for (Field field : fields) {
            boolean hasValue = false;
            try {
                field.setAccessible(true);
                if(field.get(object) != null) {
                    hasValue = true;
                }
                descriptionList.add(new Description(field.getName(), field.getType().toString(), hasValue));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
}

}
