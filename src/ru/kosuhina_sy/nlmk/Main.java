package ru.kosuhina_sy.nlmk;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        DescriptionListMaker listMaker = new DescriptionListMaker();
        List<Object> list = new ArrayList<>();
        list.add(new Person("Marge", "Simpson"));
        list.add(new Person("Homer", "Simpson","gmail@gmail.com"));

        for(List<Description> descriptions : listMaker.makeDescriptionList(list)) {
            System.out.println(descriptions.toString());
        }
    }
}
