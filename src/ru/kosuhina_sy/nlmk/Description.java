package ru.kosuhina_sy.nlmk;

public class Description {
    String parameterName;
    String parameterTypeName;
    Boolean hasValue;

    public Description(String parameterName, String parameterTypeName, Boolean hasValue) {
        this.parameterName = parameterName;
        this.parameterTypeName = parameterTypeName;
        this.hasValue = hasValue;
    }

    @Override
    public String toString() {
        return "Description{" +
                "parameterName='" + parameterName + '\'' +
                ", parameterTypeName='" + parameterTypeName + '\'' +
                ", hasValue=" + hasValue +
                '}';
    }
}
